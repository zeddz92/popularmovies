package com.fangio.edwardr.popularmovies.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.databinding.CardMovieTrailerBinding;
import com.fangio.edwardr.popularmovies.databinding.CardReviewBinding;
import com.fangio.edwardr.popularmovies.holders.ReviewViewHolder;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Review;

import java.util.ArrayList;

/**
 * Created by edwardr on 11/27/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {

    private ArrayList<Review> reviews;

    public ReviewAdapter(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        CardReviewBinding cardReviewBinding = CardReviewBinding.inflate(LayoutInflater.from(context), parent, false);

        return new ReviewViewHolder(cardReviewBinding);

    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        final Review review = reviews.get(position);
        holder.bind(review);
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }
}
