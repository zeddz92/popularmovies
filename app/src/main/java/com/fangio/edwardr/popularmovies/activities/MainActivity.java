package com.fangio.edwardr.popularmovies.activities;

import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.adapters.MoviesAdapter;
import com.fangio.edwardr.popularmovies.data.MovieContract;
import com.fangio.edwardr.popularmovies.databinding.ActivityMainBinding;
import com.fangio.edwardr.popularmovies.interfaces.ResponseListener;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Movie;
import com.fangio.edwardr.popularmovies.utils.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ListItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ArrayList<Movie> movies = new ArrayList<Movie>();
    private MoviesAdapter mAdapter;
    private static final int MOVIE_LOADER_ID = 0;
    private Boolean loaderInitialized = false;

    private String currentSort = "";

    private ActivityMainBinding mBinding;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(getString(R.string.instance_movies_key), movies);
        outState.putString(getString(R.string.instance_sort_key), currentSort);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentSort.equals(getString(R.string.path_favorite))) {
            getSupportLoaderManager().restartLoader(MOVIE_LOADER_ID, null, this);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        NetworkUtils.getInstance(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        mBinding.contentPanel.recyclerViewMovies.setLayoutManager(gridLayoutManager);
        mBinding.contentPanel.recyclerViewMovies.setHasFixedSize(true);
        mAdapter = new MoviesAdapter(movies, this);
        mBinding.contentPanel.recyclerViewMovies.setAdapter(mAdapter);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(getString(R.string.instance_movies_key))
                && savedInstanceState.containsKey(getString(R.string.instance_sort_key))) {

            ArrayList<Movie> mov = savedInstanceState.getParcelableArrayList(getString(R.string.instance_movies_key));
            movies.addAll(mov);
            currentSort = savedInstanceState.getString(getString(R.string.instance_sort_key));
            mAdapter.notifyDataSetChanged();

        } else {
            sendFetchRequest(getString(R.string.path_popular));
        }


    }

    private void sendFetchRequest(final String sort) {

        if (!sort.equals(currentSort)) {
            URL url = NetworkUtils.buildUrl(sort);
            movies.clear();
            mAdapter.notifyDataSetChanged();
            mBinding.progressBar.setVisibility(View.VISIBLE);

            NetworkUtils.getInstance().fetchMovies(this, url, new ResponseListener<ArrayList<Movie>>() {
                @Override
                public void onResponse(ArrayList<Movie> result) {

                    mBinding.progressBar.setVisibility(View.GONE);
                    movies.addAll(result);
                    mAdapter.notifyDataSetChanged();
                    currentSort = sort;
                }

                @Override
                public void onErrorResponse(String error) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();

                }
            });
            currentSort = sort;
        }
    }

    @Override
    public void onLisItemClick(int clickedItemIndex) {
        Movie movie = movies.get(clickedItemIndex);
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra(getString(R.string.intent_movie_key), movie);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_rate:
                sendFetchRequest(getString(R.string.path_top_rated));
                return true;
            case R.id.action_popular:
                sendFetchRequest(getString(R.string.path_popular));
                return true;

            case R.id.action_favorite:

                if (!currentSort.equals(getString(R.string.path_favorite)) ) {
                    currentSort = getString(R.string.path_favorite);
                    if (loaderInitialized) {
                        getSupportLoaderManager().restartLoader(MOVIE_LOADER_ID, null, this);

                    } else {
                        getSupportLoaderManager().initLoader(MOVIE_LOADER_ID, null, this);

                    }
                }


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<Cursor>(this) {

            Cursor mMovieData = null;

            @Override
            protected void onStartLoading() {
                loaderInitialized = true;
                if (mMovieData != null) {
                    // Delivers any previously loaded data immediately
                    deliverResult(mMovieData);
                } else {
                    // Force a new load
                    forceLoad();
                }
            }

            @Override
            public Cursor loadInBackground() {

                try {
                    return getContentResolver().query(MovieContract.MovieEntry.CONTENT_URI,
                            null,
                            null,
                            null,
                            MovieContract.MovieEntry._ID + " DESC"
                    );

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public void deliverResult(Cursor data) {
                mMovieData = data;
                super.deliverResult(data);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        try {
            movies.clear();
            mAdapter.notifyDataSetChanged();
            mBinding.progressBar.setVisibility(View.VISIBLE);

            while (data.moveToNext()) {
                Movie movie = new Movie();
                movie.setId(data.getInt(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_ID)));
                movie.setOverview(data.getString(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_OVERVIEW)));
                movie.setReleaseDate(data.getString(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_RELEASE_DATE)));
                movie.setVoteAverage(data.getDouble(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE)));
                movie.setPopularity(data.getDouble(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_POPULARITY)));
                movie.setOriginalTitle(data.getString(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE)));
                movie.setPosterPath(data.getString(data.getColumnIndexOrThrow(MovieContract.MovieEntry.COLUMN_POSTER_PATH)));
                movies.add(movie);

            }
        } finally {
            data.close();
            mAdapter.notifyDataSetChanged();
            mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        movies.clear();
        mAdapter.notifyDataSetChanged();
    }

}
