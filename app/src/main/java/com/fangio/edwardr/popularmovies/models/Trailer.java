package com.fangio.edwardr.popularmovies.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by edwardr on 11/27/17.
 */

public class Trailer implements Parcelable {

    private String id;
    private String name;
    private String key;
    private String type;
    private int size;

    private final static String THUMBNAIL_BASE_URL = "https://img.youtube.com/vi";

    private final static String THUMBNAIL_IMAGE_PATH = "0.jpg";



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public URL buildThumbnailUrl() {
        Uri builtUri = Uri.parse(THUMBNAIL_BASE_URL).buildUpon()
                .appendPath(key)
                .appendPath(THUMBNAIL_IMAGE_PATH)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public Trailer() {

    }

    public Trailer(String id, String name, String key, String type, int size) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.type = type;
        this.size = size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.key);
        dest.writeString(this.type);
        dest.writeInt(this.size);
    }

    private Trailer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.key = in.readString();
        this.type = in.readString();
        this.size = in.readInt();
    }

    public static final Parcelable.Creator<Trailer> CREATOR = new Parcelable.Creator<Trailer>() {
        @Override
        public Trailer createFromParcel(Parcel source) {
            return new Trailer(source);
        }

        @Override
        public Trailer[] newArray(int size) {
            return new Trailer[size];
        }
    };
}
