package com.fangio.edwardr.popularmovies.activities;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.adapters.MovieTrailersAdapter;
import com.fangio.edwardr.popularmovies.adapters.ReviewAdapter;
import com.fangio.edwardr.popularmovies.data.MovieContract;
import com.fangio.edwardr.popularmovies.databinding.ActivityMovieDetailBinding;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.interfaces.ResponseListener;
import com.fangio.edwardr.popularmovies.models.Movie;
import com.fangio.edwardr.popularmovies.models.Review;
import com.fangio.edwardr.popularmovies.models.Trailer;
import com.fangio.edwardr.popularmovies.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

public class MovieDetailActivity extends AppCompatActivity implements ListItemClickListener {


    private ActivityMovieDetailBinding mBinding;
    private Toast mToast;

    private ArrayList<Trailer> trailers = new ArrayList<Trailer>();
    private MovieTrailersAdapter mTrailersAdapter;

    private ArrayList<Review> reviews = new ArrayList<Review>();
    private ReviewAdapter mReviewsAdapter;
    private Movie movie;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(getString(R.string.instance_reviews_key), reviews);
        outState.putParcelableArrayList(getString(R.string.instance_trailers_key), trailers);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail);


        mBinding.imageButtonMarkFavorite.setTag(R.drawable.ic_heart_outline_white_36dp);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mBinding.contentTrailers.recyclerViewTrailers.setLayoutManager(linearLayoutManager);
        mBinding.contentTrailers.recyclerViewTrailers.setHasFixedSize(true);
        mTrailersAdapter = new MovieTrailersAdapter(trailers, this);
        mBinding.contentTrailers.recyclerViewTrailers.setAdapter(mTrailersAdapter);

        LinearLayoutManager reviewsLinearLayoutManager = new LinearLayoutManager(this);
        mBinding.contentReviews.recyclerViewReviews.setLayoutManager(reviewsLinearLayoutManager);
        mBinding.contentReviews.recyclerViewReviews.setHasFixedSize(true);
        mReviewsAdapter = new ReviewAdapter(reviews);
        mBinding.contentReviews.recyclerViewReviews.setAdapter(mReviewsAdapter);


        mBinding.ratingBarRating.setNumStars(10);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Intent intentThatStartedThisActivity = getIntent();
        if (intentThatStartedThisActivity.hasExtra(getString(R.string.intent_movie_key))) {
            movie = getIntent().getParcelableExtra(getString(R.string.intent_movie_key));

            mBinding.textViewTitle.setText(movie.getOriginalTitle());
            mBinding.textViewOverview.setText(movie.getOverview());
            mBinding.textViewRating.setText(String.valueOf(movie.getVoteAverage()));
            mBinding.textViewReleaseDate.setText(movie.getReleaseDate());
            mBinding.ratingBarRating.setRating(movie.getVoteAverage().floatValue());
            Picasso.with(this).load(movie.getMediumPosterPath()).into(mBinding.imageViewPosterImage);

            getSupportActionBar().setTitle(movie.getOriginalTitle());


            Uri selectUri = MovieContract.MovieEntry.CONTENT_URI
                    .buildUpon()
                    .appendPath(Integer.toString(movie.getId())).build();
            Cursor c = getContentResolver().query(selectUri, null,
                    null,
                    null,
                    null);



            if (savedInstanceState != null
                    && savedInstanceState.containsKey(getString(R.string.instance_reviews_key))
                    && savedInstanceState.containsKey(getString(R.string.instance_trailers_key))) {

                ArrayList<Review> instanceReviews = savedInstanceState.getParcelableArrayList(getString(R.string.instance_reviews_key));
                ArrayList<Trailer> instanceTrailers = savedInstanceState.getParcelableArrayList(getString(R.string.instance_trailers_key));

                reviews.addAll(instanceReviews);
                trailers.addAll(instanceTrailers);

            } else {
                fetchTrailersRequest(movie.getId());
                fetchReviewsRequest(movie.getId());
            }

            checkIfMovieInFavorite();
        }

    }

    private void fetchTrailersRequest(int movieId) {
        URL url = NetworkUtils.buildTrailersUrl(movieId);
        mBinding.progressBarTrailers.setVisibility(View.VISIBLE);

        NetworkUtils.getInstance().fetchMovieTrailers(this, url, new ResponseListener<ArrayList<Trailer>>() {
            @Override
            public void onResponse(ArrayList<Trailer> result) {
                mBinding.progressBarTrailers.setVisibility(View.GONE);

                trailers.addAll(result);
                mTrailersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorResponse(String error) {
                mBinding.progressBarTrailers.setVisibility(View.GONE);
            }
        });
    }

    private void fetchReviewsRequest(int movieId) {
        URL url = NetworkUtils.buildReviewsUrl(movieId);
        mBinding.progressBarReviews.setVisibility(View.VISIBLE);

        NetworkUtils.getInstance().fetchMovieReviews(this, url, new ResponseListener<ArrayList<Review>>() {
            @Override
            public void onResponse(ArrayList<Review> result) {
                mBinding.progressBarReviews.setVisibility(View.GONE);

                reviews.addAll(result);
                mReviewsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorResponse(String error) {
                mBinding.progressBarReviews.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }


    @Override
    public void onLisItemClick(int clickedItemIndex) {
        Trailer trailer = trailers.get(clickedItemIndex);
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + trailer.getKey()));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + trailer.getKey()));
        try {
            startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webIntent);
        }

    }

    public void onClickMarkFavorite(View view) {
        if ((Integer) mBinding.imageButtonMarkFavorite.getTag() == R.drawable.ic_heart_outline_white_36dp) {
            mBinding.imageButtonMarkFavorite.setImageResource(R.drawable.ic_heart_white_36dp);
            mBinding.imageButtonMarkFavorite.setTag(R.drawable.ic_heart_white_36dp);
            addMovieToFavorites();
        } else {
            mBinding.imageButtonMarkFavorite.setImageResource(R.drawable.ic_heart_outline_white_36dp);
            mBinding.imageButtonMarkFavorite.setTag(R.drawable.ic_heart_outline_white_36dp);
            removeMovieFromFavorites();
        }
    }

    private void addMovieToFavorites() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MovieContract.MovieEntry.COLUMN_ID, Integer.toString(movie.getId()));
        contentValues.put(MovieContract.MovieEntry.COLUMN_ORIGINAL_TITLE, movie.getOriginalTitle());
        contentValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, movie.getOverview());
        contentValues.put(MovieContract.MovieEntry.COLUMN_POSTER_PATH, movie.getPosterPath());
        contentValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, movie.getReleaseDate());
        contentValues.put(MovieContract.MovieEntry.COLUMN_POPULARITY, movie.getPopularity());
        contentValues.put(MovieContract.MovieEntry.COLUMN_POSTER_PATH, movie.getPosterPath());
        contentValues.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE, movie.getVoteAverage());

        Uri uri = getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, contentValues);

        if (uri != null) {
            if(mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(getBaseContext(), getString(R.string.toast_add_favorite), Toast.LENGTH_LONG);
            mToast.show();
        }
    }

    private void removeMovieFromFavorites() {
        Uri deleteUri = MovieContract.MovieEntry.CONTENT_URI.buildUpon().appendPath(String.valueOf(movie.getId())).build();

        int removed = getContentResolver().delete(deleteUri, null, null);
        if (removed > 0) {

            if(mToast != null) {
                mToast.cancel();
            }
            mToast =Toast.makeText(getBaseContext(), getString(R.string.toast_remove_favorite), Toast.LENGTH_LONG);
            mToast.show();

        }
    }

    private void checkIfMovieInFavorite() {
        Thread checkInFavorite = new Thread(new Runnable() {
            @Override
            public void run() {

                Uri selectUri = MovieContract.MovieEntry.CONTENT_URI
                        .buildUpon()
                        .appendPath(Integer.toString(movie.getId())).build();
                Cursor cursor = getContentResolver().query(selectUri, null,
                        null,
                        null,
                        null);

                if (!(null == cursor || cursor.getCount() == 0)) {
                    mBinding.imageButtonMarkFavorite.setImageResource(R.drawable.ic_heart_white_36dp);
                    mBinding.imageButtonMarkFavorite.setTag(R.drawable.ic_heart_white_36dp);
                }

                cursor.close();
            }
        });

        checkInFavorite.start();
    }
}
