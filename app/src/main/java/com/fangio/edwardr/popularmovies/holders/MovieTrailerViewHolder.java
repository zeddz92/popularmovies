package com.fangio.edwardr.popularmovies.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.databinding.CardMovieTrailerBinding;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Trailer;
import com.squareup.picasso.Picasso;

/**
 * Created by edwardr on 11/27/17.
 */

public class MovieTrailerViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

    private ListItemClickListener mOnClickListener;

    private CardMovieTrailerBinding mBinding;
    private View itemView;




    public MovieTrailerViewHolder(CardMovieTrailerBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
        this.itemView = binding.getRoot();

        itemView.setOnClickListener(this);
    }

    public void bind(Trailer trailer, ListItemClickListener listener) {

        mOnClickListener = listener;
        mBinding.textViewName.setText(trailer.getName());
        Picasso.with(itemView.getContext()).load(trailer.buildThumbnailUrl().toString()).into(mBinding.imageViewTrailerThumb);

    }

    @Override
    public void onClick(View view) {
        int clickedPosition = getAdapterPosition();
        mOnClickListener.onLisItemClick(clickedPosition);
    }
}
