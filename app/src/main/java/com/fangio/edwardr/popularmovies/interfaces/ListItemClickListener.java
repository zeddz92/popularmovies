package com.fangio.edwardr.popularmovies.interfaces;

/**
 * Created by edwardr on 11/25/17.
 */

public interface ListItemClickListener {
    void onLisItemClick(int clickedItemIndex);
}
