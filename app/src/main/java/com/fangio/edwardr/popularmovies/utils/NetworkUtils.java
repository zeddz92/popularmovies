package com.fangio.edwardr.popularmovies.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fangio.edwardr.popularmovies.BuildConfig;
import com.fangio.edwardr.popularmovies.interfaces.ResponseListener;
import com.fangio.edwardr.popularmovies.models.Movie;
import com.fangio.edwardr.popularmovies.models.Review;
import com.fangio.edwardr.popularmovies.models.Trailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by edwardr on 11/25/17.
 */

public class NetworkUtils {

    private static final String API_KEY = BuildConfig.API_KEY;
    private static final String BASE_URL = "http://api.themoviedb.org/3/";
    private final static String PARAM_API_KEY = "api_key";
    private final static String PATH_MOVIE = "movie";
    private final static String PATH_VIDEOS = "videos";
    private final static String PATH_REVIEWS = "reviews";


    private static NetworkUtils instance = null;
    public RequestQueue requestQueue;

    private NetworkUtils(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized NetworkUtils getInstance(Context context) {
        if (null == instance)
            instance = new NetworkUtils(context);
        return instance;
    }

    public static synchronized NetworkUtils getInstance() {
        if (null == instance) {
            throw new IllegalStateException(NetworkUtils.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return instance;
    }

    private NetworkUtils() {
    }

    public static URL buildUrl(String sort) {
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendPath(PATH_MOVIE)
                .appendPath(sort)
                .appendQueryParameter(PARAM_API_KEY, API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        Log.v("URLBUILDER", url.toString());
        return url;
    }

    public static URL buildTrailersUrl(int id) {
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendPath(PATH_MOVIE)
                .appendPath(String.valueOf(id))
                .appendPath(PATH_VIDEOS)
                .appendQueryParameter(PARAM_API_KEY, API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static URL buildReviewsUrl(int id) {
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendPath(PATH_MOVIE)
                .appendPath(String.valueOf(id))
                .appendPath(PATH_REVIEWS)
                .appendQueryParameter(PARAM_API_KEY, API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public void fetchMovies(Context context, URL url, final ResponseListener<ArrayList<Movie>> listener) {

        final ArrayList<Movie> movies = new ArrayList<Movie>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseFromServer = new JSONObject(response);
                            JSONArray moviesResults = responseFromServer.getJSONArray("results");

                            for (int i = 0; i < moviesResults.length(); i++) {

                                JSONObject movieObject = moviesResults.getJSONObject(i);

                                Movie movie = new Movie();
                                movie.setId(movieObject.getInt("id"));
                                movie.setOriginalTitle(movieObject.getString("original_title"));
                                movie.setOverview(movieObject.getString("overview"));
                                movie.setVoteAverage(movieObject.getDouble("vote_average"));
                                movie.setPosterPath(movieObject.getString("poster_path"));
                                movie.setReleaseDate(movieObject.getString("release_date"));
                                movie.setPopularity(movieObject.getDouble("popularity"));
                                movies.add(movie);
                            }

                            listener.onResponse(movies);

                        } catch (JSONException e) {
                            listener.onErrorResponse(e.getLocalizedMessage());

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onErrorResponse(error.getLocalizedMessage());

            }
        });

        requestQueue.add(stringRequest);
    }

    public void fetchMovieTrailers(Context context, URL url, final ResponseListener<ArrayList<Trailer>> listener) {
        final ArrayList<Trailer> trailers = new ArrayList<Trailer>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseFromServer = new JSONObject(response);
                            JSONArray jsonArrayResponse = responseFromServer.getJSONArray("results");

                            for (int i = 0; i < jsonArrayResponse.length(); i++) {

                                JSONObject trailerObject = jsonArrayResponse.getJSONObject(i);

                                Trailer trailer = new Trailer();
                                trailer.setId(trailerObject.getString("id"));
                                trailer.setKey(trailerObject.getString("key"));
                                trailer.setName(trailerObject.getString("name"));
                                trailer.setType(trailerObject.getString("type"));
                                trailer.setSize(trailerObject.getInt("size"));

                                trailers.add(trailer);
                            }

                            listener.onResponse(trailers);

                        } catch (JSONException e) {
                            listener.onErrorResponse(e.getLocalizedMessage());

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onErrorResponse(error.getLocalizedMessage());

            }
        });

        requestQueue.add(stringRequest);

    }


    public void fetchMovieReviews(Context context, URL url, final ResponseListener<ArrayList<Review>> listener) {
        final ArrayList<Review> reviews = new ArrayList<Review>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseFromServer = new JSONObject(response);
                            JSONArray jsonArrayResponse = responseFromServer.getJSONArray("results");

                            for (int i = 0; i < jsonArrayResponse.length(); i++) {

                                JSONObject reviewObject = jsonArrayResponse.getJSONObject(i);

                                Review review = new Review();
                                review.setId(reviewObject.getString("id"));
                                review.setAuthor(reviewObject.getString("author"));
                                review.setContent(reviewObject.getString("content"));
                                review.setUrl(reviewObject.getString("url"));

                                reviews.add(review);
                            }

                            listener.onResponse(reviews);

                        } catch (JSONException e) {
                            listener.onErrorResponse(e.getLocalizedMessage());

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onErrorResponse(error.getLocalizedMessage());

            }
        });

        requestQueue.add(stringRequest);

    }


}
