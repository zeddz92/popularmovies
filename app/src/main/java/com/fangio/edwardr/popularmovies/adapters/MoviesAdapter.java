package com.fangio.edwardr.popularmovies.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.databinding.CardMovieBinding;
import com.fangio.edwardr.popularmovies.holders.MovieViewHolder;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Movie;

import java.util.ArrayList;

/**
 * Created by edwardr on 11/25/17.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private ArrayList<Movie> movies;
    private final ListItemClickListener mOnClickListener;


    public MoviesAdapter(ArrayList<Movie> movies, ListItemClickListener listener) {
        this.movies = movies;
        this.mOnClickListener = listener;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        CardMovieBinding cardMovieBinding = CardMovieBinding.inflate(LayoutInflater.from(context), parent, false);

        return new MovieViewHolder(cardMovieBinding);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        final Movie movie = movies.get(position);
        holder.bind(movie, mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
