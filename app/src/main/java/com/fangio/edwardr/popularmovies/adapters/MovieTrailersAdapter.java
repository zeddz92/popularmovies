package com.fangio.edwardr.popularmovies.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fangio.edwardr.popularmovies.databinding.CardMovieTrailerBinding;
import com.fangio.edwardr.popularmovies.holders.MovieTrailerViewHolder;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Trailer;

import java.util.ArrayList;

/**
 * Created by edwardr on 11/27/17.
 */

public class MovieTrailersAdapter  extends RecyclerView.Adapter<MovieTrailerViewHolder> {

    private ArrayList<Trailer> trailers;
    private final ListItemClickListener mOnClickListener;


    public MovieTrailersAdapter(ArrayList<Trailer> trailers, ListItemClickListener listener) {
        this.trailers = trailers;
        this.mOnClickListener = listener;
    }

    @Override
    public MovieTrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        CardMovieTrailerBinding cardMovieTrailerBinding = CardMovieTrailerBinding.inflate(LayoutInflater.from(context), parent, false);

        return new MovieTrailerViewHolder(cardMovieTrailerBinding);
    }

    @Override
    public void onBindViewHolder(MovieTrailerViewHolder holder, int position) {
        final Trailer trailer = trailers.get(position);
        holder.bind(trailer, mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return trailers.size();
    }
}
