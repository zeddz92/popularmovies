package com.fangio.edwardr.popularmovies.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.databinding.CardReviewBinding;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Movie;
import com.fangio.edwardr.popularmovies.models.Review;

/**
 * Created by edwardr on 11/27/17.
 */

public class ReviewViewHolder extends RecyclerView.ViewHolder {

    private CardReviewBinding mBinding;

    public ReviewViewHolder(CardReviewBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public void bind(Review review) {

        mBinding.textViewAuthorInitials.setText(review.getAuthorInitials());
        mBinding.textViewAuthorName.setText(review.getAuthor());
        mBinding.textViewAuthorBy.setText(review.getAuthor());
        mBinding.textViewContent.setText(review.getContent());


    }
}
