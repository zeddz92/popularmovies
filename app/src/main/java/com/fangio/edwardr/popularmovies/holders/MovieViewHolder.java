package com.fangio.edwardr.popularmovies.holders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fangio.edwardr.popularmovies.R;
import com.fangio.edwardr.popularmovies.databinding.CardMovieBinding;
import com.fangio.edwardr.popularmovies.interfaces.ListItemClickListener;
import com.fangio.edwardr.popularmovies.models.Movie;
import com.squareup.picasso.Picasso;

/**
 * Created by edwardr on 11/25/17.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ListItemClickListener mOnClickListener;
    private CardMovieBinding mBinding;
    private View itemView;
    public MovieViewHolder(CardMovieBinding binding) {
        super(binding.getRoot());
        this.itemView = binding.getRoot();
        this.mBinding = binding;

        itemView.setOnClickListener(this);
    }

    public void bind(Movie movie, ListItemClickListener listener) {
        mBinding.texViewTitle.setText(movie.getOriginalTitle());
        mBinding.textViewRating.setText(String.valueOf(movie.getVoteAverage()));

        mOnClickListener = listener;
        Picasso.with(itemView.getContext()).load(movie.getMediumPosterPath()).into(mBinding.imageViewPosterImage);

    }

    @Override
    public void onClick(View view) {
        int clickedPosition = getAdapterPosition();
        mOnClickListener.onLisItemClick(clickedPosition);
    }
}
