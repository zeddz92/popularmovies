package com.fangio.edwardr.popularmovies.interfaces;

/**
 * Created by edwardr on 11/26/17.
 */

public interface ResponseListener<T> {
    public void onResponse(T object);
    public void onErrorResponse(String error);

}
