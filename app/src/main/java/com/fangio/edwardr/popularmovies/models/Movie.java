package com.fangio.edwardr.popularmovies.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by edwardr on 11/25/17.
 */

public class Movie implements Parcelable {

    private int id;
    private String originalTitle;
    private String overview;
    private Double popularity;
    private String posterPath;
    private Double voteAverage;
    private String releaseDate;
    private final static String POSTER_BASE_URL = "http://image.tmdb.org/t/p/";
    private final static String POSTER_PATH_SIZE_SMALL = "w185";
    private final static String POSTER_PATH_SIZE_MEDIUM = "w500";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getMediumPosterPath() {
        return POSTER_BASE_URL + POSTER_PATH_SIZE_MEDIUM + posterPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }


    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Movie() {

    }

    public Movie(int id, String originalTitle, String overview, Double popularity, String posterPath, Double voteAverage, String releaseDate) {
        this.id = id;
        this.originalTitle = originalTitle;
        this.overview = overview;
        this.popularity = popularity;
        this.posterPath = posterPath;
        this.voteAverage = voteAverage;
        this.releaseDate = releaseDate;
    }

    private Movie(Parcel in) {
        this.id = in.readInt();
        this.originalTitle = in.readString();
        this.overview = in.readString();
        this.popularity = in.readDouble();
        this.posterPath = in.readString();
        this.voteAverage = in.readDouble();
        this.releaseDate = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(originalTitle);
        parcel.writeString(overview);
        parcel.writeDouble(popularity);
        parcel.writeString(posterPath);
        parcel.writeDouble(voteAverage);
        parcel.writeString(releaseDate);

    }
}
