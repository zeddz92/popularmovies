package com.fangio.edwardr.popularmovies.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by edwardr on 11/27/17.
 */

public class MovieContract {

    public static final String AUTHORITY = "com.fangio.edwardr.popularmovies";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_MOVIES = "movies";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INT";
    private static final String FLOAT_TYPE = " FLOAT";
    private static final String DOUBLE_TYPE = " DOUBLE";

    private static final String COMMA_SEP = ",";


    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MovieEntry.TABLE_NAME + " (" +
                    MovieEntry._ID + " INTEGER PRIMARY KEY," +
                    MovieEntry.COLUMN_ID + " UNIQUE" + COMMA_SEP + INT_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_ORIGINAL_TITLE  + TEXT_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_OVERVIEW + TEXT_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_POPULARITY + DOUBLE_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_POSTER_PATH + TEXT_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_VOTE_AVERAGE + DOUBLE_TYPE + COMMA_SEP +
                    MovieEntry.COLUMN_RELEASE_DATE + TEXT_TYPE + " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MovieEntry.TABLE_NAME;

    public static final class MovieEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIES).build();


        public static final String TABLE_NAME = "movies";

        public static final String COLUMN_ID = "movieId";
        public static final String COLUMN_ORIGINAL_TITLE = "originalTitle";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_POPULARITY = "popularity";
        public static final String COLUMN_POSTER_PATH = "posterPath";
        public static final String COLUMN_VOTE_AVERAGE = "voteAverage";
        public static final String COLUMN_RELEASE_DATE = "releaseDate";


    }
}
